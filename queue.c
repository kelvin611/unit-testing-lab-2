#include <stdio.h>
#include "queue.h"

#define QUEUE_MAX 100
//typedef struct {
//	
//	
//	uint8_t queue_memory[100];
//	int front;
//	int rear;
//	int size;
//
//	// todo: add more members as needed
//} queue_s;

void queue__init(queue_S *queue) {
	queue->front = 0;
	queue->rear =-1;
	queue->size = 0;
}

bool queue__push(queue_S *queue, uint8_t push_value)
{

  if (queue->size == QUEUE_MAX)
  {
    return false;
  }
  else
  {
    if (queue->rear == QUEUE_MAX - 1)
    {
      queue->rear = -1;
    }

    queue->rear += 1;
    queue->queue_memory[queue->rear] = push_value;
    queue->size += 1;

    return true;
  }
}

bool queue__pop(queue_S *queue, uint8_t *pop_value)
{

  if (queue->size == 0)
  {
    return false;
  }

  else
  {
    if (queue->front == QUEUE_MAX)
    {
      queue->front = 0;
    }
    *pop_value = queue->queue_memory[queue->front];
    queue->front += 1;
    queue->size -= 1;
    return true;
  }
}

size_t queue__get_count(queue_S *queue) {
	return queue->size;
}